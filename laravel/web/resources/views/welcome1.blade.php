@extends('tugas_5.master')

@section('judul')
    Halaman Index
@endsection

@section('content')
        <h1>Selamat Datang, {{$first}} {{$last}}!</h1>
        <h3>Terima kasih telah bergabung di Website Kami. Media belajar kita bersama!</h3>
@endsection