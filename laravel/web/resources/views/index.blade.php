@extends('tugas_5.master')

@section('judul')
    Halaman Home
@endsection

@section('content')
<h1>Media Online</h1>
<h3>Sosial Media Developer</h3>
<p>Belajar dan berbagi agar hidup jadi lebih baik</p>
<b>Benefit join di Media Online</b>
<ul>
    <li>Mendapat motivasi dari sesama Developer</li>
    <li>Sharing knowledge</li>
    <li>Dibuat oleh calon Developer terbaik</li>
</ul>
<b>Cara bergabung ke Media Online</b>
<ol>
    <li>Mengunjungi website ini</li>
    <li>Mendaftarkan diri di <a href='/register'>Form sign up</a></li>
    <li>Selesai</li>
</ol>
@endsection
