@extends('tugas_5.master')

@section('judul')
    Halaman Form
@endsection

@section('content')
        <h1>Buat Account Baru</h1>
        <h3>Sign Up Form</h3>
        <br>
        <form action='/welcome' method="post">
            @csrf
        <label>First Name:</label><br>
        <input type="text" name="first_name"><br><br>
        <label>Last Name:</label><br>
        <input type="text" name="last_name"><br><br>
        <label>Gender</label><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br><br>
        <label>Nationality</label><br>
        <select name="national">
            <option value="indonesia">Indonesian</option>
            <option value="amerika">American</option>
            <option value="arab">Arabian</option>
            <option value="other">Other</option>
        </select><br><br>
        <label>Language Spoken</label><br>
        <input type="checkbox" name="language">Bahasa Indonesia <br>
        <input type="checkbox" name="language">English <br>
        <input type="checkbox" name="language">العربية <br>
        <input type="checkbox" name="language">Other<br><br>
        <label>Bio</label><br>
        <textarea name="bio" rows="15" cols="30"></textarea><br><br>
        <input type="submit">
        
        </form>
@endsection