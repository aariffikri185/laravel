<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;

class CastController extends Controller
{
    public function index()
    {
        $post = Cast::all();
        return view('tugas_6.index', compact('post'));
    }

    public function create()
    {
    	return view('tugas_6.create');
    }
 
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'nama' => 'required',
    		'umur' => 'required',
            'bio' => 'required'
    	]);
 
        Cast::create([
    		'nama' => $request->nama,
    		'umur' => $request->umur,
            'bio' => $request->bio
    	]);
 
    	return redirect('/cast');
    }

    public function show($id)
    {
        $cast = Cast::where('id', $id)->first();
        return view('tugas_6.show', compact('cast'));
    }

    
public function edit($id)
{
    $cast = Cast::where('id',$id)->first();
    return view('tugas_6.edit', compact('cast'));
}

public function update(Request $request, $id)
{
    $request->validate([
        'nama' => 'required|unique:cast',
        'umur' => 'required',
        'bio' => 'required',
    ]);

    $cast = Cast::find($id);
    $cast->nama = $request['nama'];
    $cast->umur = $request['umur'];
    $cast->bio = $request['bio'];
    $cast->save();
    return redirect('/cast');
}


public function destroy($id)
    {
        $cast = Cast::find($id);
        $cast->delete();
        return redirect('/cast');
    }
}
