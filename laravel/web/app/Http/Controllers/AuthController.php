<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('form');
    }

    public function send(Request $request){
        $first = $request['first_name'];    
        $last = $request['last_name'];
        return view('welcome1', compact('first','last'));
    }
}
