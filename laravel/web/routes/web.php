<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@send');

Route::get('/master', function(){
    return view ('tugas_5.master');
});

Route::get('/data-table', function(){
    return view ('tugas_5.table.data');
});

//CRUD Tugas 2 Pekan 6
// Create Umum
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');

// Read
Route::get('/cast', 'CastController@index');
Route::get('/cast/{cast_id}', 'CastController@show');

//Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');

// Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
